package server;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import online.chazi.netkit.channel.NetkitChannelEvent;
import online.chazi.netkit.messaging.Message;
import online.chazi.netkit.listener.NetkitChanelListener;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 16:34
 */
@Slf4j
public class EventListener implements NetkitChanelListener {
    @Override
    public void onRegistered(NetkitChannelEvent event) {
        System.out.println("onRegistered..");
    }

    @Override
    public void onUnregistered(NetkitChannelEvent event) {
        System.out.println("onUnregistered..");
    }

    @Override
    public void onExceptionCaught(NetkitChannelEvent event, Throwable throwable) {
        System.out.println("onExceptionCaught..");
    }

    @Override
    public void onMessageReceived(NetkitChannelEvent event, Message message) {
        log.debug("收到消息：{}", JSON.toJSONString(message));
    }
}
