package server.action;

import common.ActionSupport;
import common.MessageID;
import online.chazi.netkit.messaging.Message;
import online.chazi.netkit.exception.ActionException;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 16:49
 */
public class SayHelloAction extends ActionSupport {

    @Override
    public void execute() throws ActionException {

        sendMessage(new Message(MessageID.SAY_HELLO.id, "你好！我是服务器。你说：[" + getMessage() + "]"));

    }

}
