package server;

import lombok.extern.slf4j.Slf4j;
import online.chazi.netkit.Session;
import online.chazi.netkit.listener.NetkitSessionListener;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 16:35
 */
@Slf4j
public class SessionListener implements NetkitSessionListener {

    @Override
    public void onCreated(Session session) {
        log.debug("开启了一个新的会话{}", session.getChannel().id());
    }

    @Override
    public void onClosed(Session session) {
        log.debug("关闭了一个会话:{}", session.getChannel().id());
    }

}
