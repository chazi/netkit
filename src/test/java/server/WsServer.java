package server;

import common.MessageID;
import online.chazi.netkit.NetkitContext;
import online.chazi.netkit.NetkitServer;
import online.chazi.netkit.action.Action;
import online.chazi.netkit.action.ActionChain;
import online.chazi.netkit.action.ActionFilter;
import online.chazi.netkit.action.DoNotingAction;
import online.chazi.netkit.exception.ActionException;
import online.chazi.netkit.messaging.DefaultMessageEncrypt;
import online.chazi.netkit.messaging.Message;
import server.action.HeartbeatAction;
import server.action.SayHelloAction;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-06-18 15:17
 */
public class WsServer {

    public static void main(String[] args) {

        NetkitContext context = SessionHolder.getContext();
        init(context);

        NetkitServer server = new NetkitServer(context, "0.0.0.0", 2688);
        server.setMaxConnections(3);
        server.startupWebSocket("/websocket");

    }

    private static void init(NetkitContext context) {
        context.addActionFilter(new ActionFilter() {
            @Override
            public void doFilter(Action action, Message message, ActionChain chain) throws ActionException {
                System.out.println("它来了~~~");
                chain.doChain(action, message);
            }
        });
        context.setChanelListener(new EventListener());
        context.setSessionListener(new SessionListener());
        context.setMessageEncrypt(new DefaultMessageEncrypt());
        registerActions(context);
    }

    private static void registerActions(NetkitContext context) {

        context.setNotFoundAction(new DoNotingAction());

        context.registerAction(MessageID.HEARTBEAT.id, HeartbeatAction.class);
        context.registerAction(MessageID.SAY_HELLO.id, SayHelloAction.class);

    }

}
