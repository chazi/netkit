package common;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 15:56
 */
public enum MessageID {

    HEARTBEAT(1, "心跳"),
    HEARTBEAT_RESPONSE(2, "心跳响应"),
    CLOSE(10, "关闭连接"),
    LOGOUT(11, "退出登录"),
    ERROR(12, "服务异常"),
    SAY_HELLO(8888, "打招呼，呵呵");

    public int id;
    public String meaning;

    MessageID(int id, String meaning) {
        this.id = id;
        this.meaning = meaning;
    }

}
