package common;

import lombok.extern.slf4j.Slf4j;
import online.chazi.netkit.action.SocketAction;
import online.chazi.netkit.channel.NetkitFuture;
import online.chazi.netkit.exception.ActionException;
import online.chazi.netkit.messaging.Message;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 16:41
 */
@Slf4j
public abstract class ActionSupport extends SocketAction {

    private int messageId;
    private String message;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void execute(Message message) throws ActionException {

        this.messageId = message.getMessageId();
        this.message = message.messageContentAsString();
        try {
            execute();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            sendMessage(new Message(MessageID.ERROR.id));
        }

    }

    public abstract void execute() throws ActionException;

    public NetkitFuture sendMessage(Message message) {
        return super.getSession().sendMessage(message);
    }

}
