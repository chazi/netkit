import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-26 22:38
 */
public class Test {

    public static void main(String[] args) {

        Map<String, Map<Integer, String>> m = new ConcurrentHashMap<>();

        for (int i = 0; i < 3; i++) {

            Map<Integer, String> e = m.get("aa");
            if (e == null) {
                e = new HashMap<>();
                m.put("aa", e);
            } else {
                System.out.println("e=" + JSON.toJSONString(e));
            }

            e.put(2 + i, "22," + i);
            e.put(3 + i, "33," + i);

        }

        Map<Integer, String> e = m.get("aa");
        System.out.println("e=" + JSON.toJSONString(e));

    }

}
