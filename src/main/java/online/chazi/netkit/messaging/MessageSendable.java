package online.chazi.netkit.messaging;

import online.chazi.netkit.channel.NetkitFuture;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-23 18:05
 */
public interface MessageSendable {

    NetkitFuture sendMessage(Message message);

}
