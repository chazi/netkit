package online.chazi.netkit.messaging;

import java.io.Serializable;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-23 18:06
 */
public class Message implements Serializable {

    public static final int HEADER_SIZE = 9;
    private int messageId;
    private byte extension;
    private byte[] messageContent;

    public Message() {
    }

    public Message(int messageId) {
        this.messageId = messageId;
    }

    public Message(int messageId, String data) {
        this.messageId = messageId;
        this.messageContent = data.getBytes();
    }

    public Message(int messageId, byte[] messageContent) {
        this.messageId = messageId;
        this.messageContent = messageContent;
    }

    public Message(int messageId, byte extension, byte[] messageContent) {
        this.messageId = messageId;
        this.extension = extension;
        this.messageContent = messageContent;
    }

    public int getMessageId() {
        return this.messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public byte getExtension() {
        return extension;
    }

    public void setExtension(byte extension) {
        this.extension = extension;
    }

    public byte[] getMessageContent() {
        return this.messageContent;
    }

    public void setMessageContent(byte[] messageContent) {
        this.messageContent = messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent.getBytes();
    }

    public String messageContentAsString() {
        return this.messageContent == null ? null : new String(this.messageContent);
    }

    public int getPacketSize() {
        return this.messageContent == null ? HEADER_SIZE : HEADER_SIZE + this.messageContent.length;
    }

    public int getContentLength() {
        return this.messageContent == null ? 0 : this.messageContent.length;
    }

}
