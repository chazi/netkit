package online.chazi.netkit.messaging;

import online.chazi.netkit.Session;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 16:11
 */
public class MessageTask implements Runnable {

    private final MessageDispatcher dispatcher;
    private Session session;
    private Message message;

    private MessageTask(MessageDispatcher dispatcher, Session session, Message msg) {
        this.dispatcher = dispatcher;
        this.session = session;
        this.message = msg;
    }

    public void run() {
        this.execute();
    }

    public void execute() {
        synchronized (this.dispatcher) {
            this.dispatcher.dispatch(this.session.getContext(), this.session, this.message);
        }
    }

    public static MessageTask newInstance(MessageDispatcher dispatcher, Session session, Message msg) {
        return new MessageTask(dispatcher, session, msg);
    }

}