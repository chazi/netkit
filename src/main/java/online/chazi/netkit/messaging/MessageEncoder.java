package online.chazi.netkit.messaging;

import java.nio.ByteBuffer;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:03
 */
public class MessageEncoder {

    public static byte[] encode(Message message) {
        ByteBuffer buffer = ByteBuffer.allocate(message.getPacketSize());
        encode(buffer, message);
        byte[] bytes = buffer.array();
        buffer.clear();
        return bytes;
    }

    public static void encode(ByteBuffer buffer, Message message) {
        buffer.putInt(message.getMessageId());
        buffer.put(message.getExtension());
        buffer.putInt(message.getContentLength());
        if (message.getContentLength() > 0) {
            buffer.put(message.getMessageContent());
        }
    }

}
