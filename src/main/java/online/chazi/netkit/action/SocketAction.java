package online.chazi.netkit.action;

import online.chazi.netkit.Session;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 16:40
 */
public abstract class SocketAction implements Action {

    private Session session;

    public Session getSession() {
        return this.session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

}
