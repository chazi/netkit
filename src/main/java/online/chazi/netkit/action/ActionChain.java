package online.chazi.netkit.action;

import online.chazi.netkit.exception.ActionException;
import online.chazi.netkit.messaging.Message;

import java.util.Iterator;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 10:55
 */
public class ActionChain {

    private Iterator<ActionFilter> iterator;

    public ActionChain() {
    }

    public void setIterator(Iterator<ActionFilter> it) {
        this.iterator = it;
    }

    public void doChain(Action action, Message message) throws ActionException {
        if (this.iterator.hasNext()) {
            this.iterator.next().doFilter(action, message, this);
        } else {
            action.execute(message);
        }
    }

}
