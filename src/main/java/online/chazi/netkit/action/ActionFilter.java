package online.chazi.netkit.action;

import online.chazi.netkit.exception.ActionException;
import online.chazi.netkit.messaging.Message;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 10:56
 */
public interface ActionFilter {
    void doFilter(Action action, Message message, ActionChain chain) throws ActionException;
}
