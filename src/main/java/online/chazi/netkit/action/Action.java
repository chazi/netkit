package online.chazi.netkit.action;

import online.chazi.netkit.Session;
import online.chazi.netkit.exception.ActionException;
import online.chazi.netkit.messaging.Message;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 10:54
 */
public interface Action {

    void execute(Message message) throws ActionException;

    Session getSession();

    void setSession(Session session);

}
