package online.chazi.netkit.exception;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 10:55
 */
public class ActionException extends Exception {

    public ActionException() {
    }

    public ActionException(String message) {
        super(message);
    }

    public ActionException(Throwable e) {
        super(e);
    }

    public ActionException(String message, Throwable e) {
        super(message, e);
    }
}