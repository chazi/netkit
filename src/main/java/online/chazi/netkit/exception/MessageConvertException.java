package online.chazi.netkit.exception;

import java.io.IOException;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:03
 */
public class MessageConvertException extends IOException {

    public MessageConvertException() {
    }

    public MessageConvertException(String message) {
        super(message);
    }

}