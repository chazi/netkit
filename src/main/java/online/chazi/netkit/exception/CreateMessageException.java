package online.chazi.netkit.exception;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:04
 */
public class CreateMessageException extends Exception {

    public CreateMessageException() {
    }

    public CreateMessageException(String message) {
        super(message);
    }

    public CreateMessageException(Throwable e) {
        super(e);
    }

    public CreateMessageException(String message, Throwable e) {
        super(message, e);
    }

}