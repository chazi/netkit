package online.chazi.netkit.exception;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:04
 */
public class SessionException extends RuntimeException {

    public SessionException() {
    }

    public SessionException(String message) {
        super(message);
    }

    public SessionException(Throwable e) {
        super(e);
    }

    public SessionException(String message, Throwable e) {
        super(message, e);
    }

}
