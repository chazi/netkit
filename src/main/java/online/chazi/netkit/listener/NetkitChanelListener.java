package online.chazi.netkit.listener;

import online.chazi.netkit.channel.NetkitChannelEvent;
import online.chazi.netkit.messaging.Message;

import java.util.EventListener;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:07
 */
public interface NetkitChanelListener extends EventListener {

    void onRegistered(NetkitChannelEvent event);

    void onUnregistered(NetkitChannelEvent event);

    void onExceptionCaught(NetkitChannelEvent event, Throwable throwable);

    void onMessageReceived(NetkitChannelEvent event, Message message);

}