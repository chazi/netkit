package online.chazi.netkit.listener;

import lombok.extern.slf4j.Slf4j;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-27 12:11
 */
@Slf4j
public class DefaultStartUpListener implements StartUpListener {
    @Override
    public void startup(boolean success) {
        if (success) log.info("Server startup!");
    }
}
