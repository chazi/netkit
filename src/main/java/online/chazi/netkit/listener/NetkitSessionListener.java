package online.chazi.netkit.listener;

import online.chazi.netkit.Session;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 15:05
 */
public interface NetkitSessionListener {

    /**
     * 当一个新的会话创建后，会调用该方法。
     * 在超大型聊天系统中，应该在方法调用后，使用
     * session.getSessionId() 得到会话id，纪录在redis来映射server与session之间的关系
     * 方便做路由，中转消息
     *
     * @param session 当前建立的会话
     */
    void onCreated(Session session);

    /**
     * 会话关闭之后调用的方法。
     * 应该在redis中移除server与session的映射关系。
     *
     * @param session 当前建立的会话
     */
    void onClosed(Session session);

}
