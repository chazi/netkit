package online.chazi.netkit.annotation;

import online.chazi.netkit.action.Action;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-26 16:37
 */
public interface InjectProxy {
    void inject(Action action) throws Exception;
}
