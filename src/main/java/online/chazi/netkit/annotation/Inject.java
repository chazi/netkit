package online.chazi.netkit.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-26 16:38
 */
@Target({TYPE, FIELD})
@Retention(RUNTIME)
public @interface Inject {
    String name() default "";
}