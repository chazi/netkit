package online.chazi.netkit.channel;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import online.chazi.netkit.Session;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:52
 */
public abstract class NetkitFutureListener implements ChannelFutureListener {

    private Session session;

    public NetkitFutureListener() {
    }

    protected void setSession(Session session) {
        this.session = session;
    }

    public void operationComplete(ChannelFuture channelFuture) throws Exception {
        this.onComplete(channelFuture.isSuccess(), this.session);
    }

    public abstract void onComplete(boolean success, Session session);

}