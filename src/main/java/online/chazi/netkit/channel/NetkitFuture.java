package online.chazi.netkit.channel;

import io.netty.channel.ChannelFuture;
import online.chazi.netkit.Session;

import java.util.concurrent.TimeUnit;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 11:49
 */
public class NetkitFuture {

    private ChannelFuture channelFuture;
    private Session session;

    public NetkitFuture(Session session, ChannelFuture channelFuture) {
        this.session = session;
        this.channelFuture = channelFuture;
    }

    public void addFutureListener(NetkitFutureListener listener) {
        this.channelFuture.addListener(listener);
    }

    public void removeFutureListener(NetkitFutureListener listener) {
        this.channelFuture.removeListener(listener);
    }

    public Session getSession() {
        return this.session;
    }

    public boolean isDone() {
        return this.channelFuture.isDone();
    }

    public boolean isCancelled() {
        return this.channelFuture.isCancelled();
    }

    public boolean isSuccess() {
        return this.channelFuture.isCancelled();
    }

    public Throwable cause() {
        return this.channelFuture.cause();
    }

    public boolean cancel() {
        return this.channelFuture.cancel(true);
    }

    public ChannelFuture await() throws InterruptedException {
        return this.channelFuture.await();
    }

    public ChannelFuture awaitUninterruptibly() {
        return this.channelFuture.awaitUninterruptibly();
    }

    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        return this.channelFuture.await(timeout, unit);
    }

    public boolean await(long timeoutMillis) throws InterruptedException {
        return this.channelFuture.await(timeoutMillis);
    }

    public boolean awaitUninterruptibly(long timeout, TimeUnit unit) {
        return this.channelFuture.awaitUninterruptibly(timeout, unit);
    }

    public boolean awaitUninterruptibly(long timeoutMillis) {
        return this.channelFuture.awaitUninterruptibly(timeoutMillis);
    }

}
