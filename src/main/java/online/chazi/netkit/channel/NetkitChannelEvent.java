package online.chazi.netkit.channel;

import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import online.chazi.netkit.NetkitContext;

import java.util.EventObject;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 14:48
 */
public class NetkitChannelEvent extends EventObject {

    private NetkitContext serverContext;
    private Channel channel;

    public NetkitChannelEvent(NetkitContext serverContext, Channel channel) {
        super(channel);
        this.serverContext = serverContext;
        this.channel = channel;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public ChannelId getChannelID() {
        return this.getChannel().id();
    }

    public NetkitContext getServerContext() {
        return this.serverContext;
    }

}