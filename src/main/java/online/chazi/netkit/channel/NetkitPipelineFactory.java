package online.chazi.netkit.channel;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import online.chazi.netkit.NetkitContext;
import online.chazi.netkit.NetkitHandler;
import online.chazi.netkit.messaging.MessageProtocolDecoder;
import online.chazi.netkit.messaging.MessageProtocolEncoder;

import java.util.concurrent.TimeUnit;

/**
 * 说明：作者很懒，什么都没留下
 * Created by 叉子同学 on 2020-04-24 14:17
 */
public class NetkitPipelineFactory extends ChannelInitializer<SocketChannel> {

    private NetkitContext context;

    public NetkitPipelineFactory(NetkitContext context) {
        this.context = context;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        long timeoutMillis = context.getSessionTimeout().toMillis();
        socketChannel.pipeline()
                .addLast(new ChunkedWriteHandler())
                .addLast(new IdleStateHandler(timeoutMillis, timeoutMillis, timeoutMillis, TimeUnit.MILLISECONDS))
                .addLast("decoder", new MessageProtocolDecoder())
                .addLast("encoder", new MessageProtocolEncoder())
                .addLast(new NetkitHandler(context));
    }

}
